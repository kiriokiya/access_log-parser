<?php

namespace Classes;

use Classes\Abstracted\LogParser;

/**
 * @property AccessLogParseResponse $response
 */
class AccessLogParser extends LogParser
{
    protected $regex = "/(\S+) (\S+) (\S+) \[([^:]+):(\d+:\d+:\d+) ([^\]]+)\] \"(\S+) (.*?) (\S+)\" (\S+) (\S+) (\".*?\") (\".*?\")/";

    public function parseLogRow(string $row, int $rowNumber): void
    {
        preg_match($this->regex, $row, $result);

        try {
            $this->validateResult($result, $rowNumber);
            $this->response->views++;
            $this->response->addRawUrls($result[8]);
            $this->response->addTraffic($result[10], $result[11]);
            $this->response->crawlers->addElement($result[13]);
            $this->response->addStatusCode($result[10]);
        } catch (\Throwable $e) {
            echo $e->getMessage() . PHP_EOL;
        }
    }

    protected function validateResult(array $result, int $rowNumber): void
    {
       if (count($result) !== 14) {
           throw new \Exception("Не удалось распарсить строку {$rowNumber}. Строка {$rowNumber} не включена в итоговые значения");
       }
    }
}