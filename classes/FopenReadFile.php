<?php

namespace Classes;

use Classes\Abstracted\ReadFileInterface;

class FopenReadFile extends ReadFileInterface
{
    public function getRow(): \Generator
    {
        while(($row = fgets($this->file)) !== false) {
            yield $row;
        }
    }

    protected function openFile(string $fileName)
    {
        return fopen($fileName, "r");
    }
}