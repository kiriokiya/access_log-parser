<?php

namespace Classes;

class CrawlersCount
{
    /** @var int */
    public $Google = 0;
    /** @var int */
    public $Bing = 0;
    /** @var int */
    public $Baidu = 0;
    /** @var int */
    public $Yandex = 0;

    public function addElement(string $meta): void
    {
        foreach ($this as $key => $value) {
            if (strpos($meta, $key) !== false) {
                $this->$key++;
                break;
            }
        }
    }
}