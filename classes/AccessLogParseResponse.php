<?php

namespace Classes;

use Classes\Abstracted\ParsedLogResponse;

class AccessLogParseResponse extends ParsedLogResponse
{
    protected const REDIRECTION = 300;
    protected const CLIENT_ERROR  = 400;
    /** @var int */
    public $views = 0;
    /** @var int */
    public $urls = 0;
    /** @var int */
    public $traffic = 0;
    /** @var CrawlersCount */
    public $crawlers;
    /** @var int[] */
    public $statusCodes = [];
    /** @var array */
    protected $rawUrls = [];

    public function __construct()
    {
        $this->crawlers = new CrawlersCount();
    }

    public function prepareResponse(): ParsedLogResponse
    {
        $this->urls = count($this->rawUrls);
        return $this;
    }

    public function addRawUrls(string $url): void
    {
        if (!array_key_exists($url, $this->rawUrls)) {
            $this->rawUrls[$url] = null;
        }
    }

    public function addStatusCode(string $statusCode): void
    {
        if (array_key_exists($statusCode, $this->statusCodes)) {
            $this->statusCodes[$statusCode]++;
        } else {
            $this->statusCodes[$statusCode] = 1;
        }
    }

    public function addTraffic(int $statusCode, int $traffic): void
    {
        if ($this->checkStatusCode($statusCode)) {
            $this->traffic += $traffic;
        }
    }

    protected function checkStatusCode(int $statusCode): bool
    {
        return $statusCode < AccessLogParseResponse::REDIRECTION || $statusCode >= AccessLogParseResponse::CLIENT_ERROR;
    }
}
