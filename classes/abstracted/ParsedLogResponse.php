<?php

namespace Classes\Abstracted;

/**
 * Информация о распаршенном логе
 */
abstract class ParsedLogResponse
{
    /**
     * Подготовливает объект к отдаче
     */
    abstract public function prepareResponse(): ParsedLogResponse;
}