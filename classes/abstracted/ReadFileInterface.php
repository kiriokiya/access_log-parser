<?php

namespace Classes\Abstracted;

/**
 * Абстракстный класс для различных реализаций чтений файла
 */
abstract class ReadFileInterface
{
    protected $file;

    public function __construct(string $fileName)
    {
        $this->checkFileExist($fileName);
        $this->file = $this->openFile($fileName);
    }

    protected function checkFileExist(string $fileName): void
    {
        if (!file_exists(__DIR__ . "/../../{$fileName}")) {
            throw new \Exception('Указанного файла не существует');
        }
    }

    abstract public function getRow();

    abstract protected function openFile(string $fileName);
}