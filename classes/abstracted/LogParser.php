<?php

namespace Classes\Abstracted;

/**
 * Класс для обработки строки лога
 */
abstract class LogParser
{
    /** @var ParsedLogResponse */
    protected $response;

    public function __construct(ParsedLogResponse $response)
    {
        $this->response = $response;
    }

    abstract public function parseLogRow(string $row, int $rowNumber): void;

    public function getResponse(): ParsedLogResponse
    {
        return $this->response;
    }
}