<?php

use Classes\AccessLogParser;
use Classes\AccessLogParseResponse;
use Classes\FopenReadFile;

require_once __DIR__ . '/vendor/autoload.php';

try {
    $fileService = new FopenReadFile($argv[1]);
    $parser = new AccessLogParser(new AccessLogParseResponse());

    foreach ($fileService->getRow() as $number => $row) {
        $parser->parseLogRow($row, $number);
    }
    $response = $parser->getResponse()->prepareResponse();
    echo \json_encode($response, JSON_PRETTY_PRINT);
} catch (Throwable $e) {
    print $e->getMessage() . PHP_EOL;
}

